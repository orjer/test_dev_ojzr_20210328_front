import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Persona } from '../../interfaces/Persona';


@Injectable({
  providedIn: 'root'
})
export class ApiPersonasService {

    base_path = 'http://localhost:50335/api';
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }

    constructor(private http: HttpClient) { }

    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('Ocurrión un error:', error.error.message);
        }else{
            console.error(
                `Código de error ${error.status}, ` +
                `Mensaje: ${error.error}`);
        }
        return throwError('Ocurrió un error; favor de intentar más tarde.');
    }

    getPersonas(): Observable<any> {
        return this.http
                .get(this.base_path + '/PersonasFisicas' )
                .pipe(
                    retry(2),
                    catchError(this.handleError)
                )
    }

    crearPersona(persona:Persona): Observable<any> {
        return this.http
                .post(this.base_path + '/PersonasFisicas', persona, this.httpOptions)
                .pipe(
                    retry(2),
                    catchError(this.handleError)
                )
    }

    modificarPersona(persona:Persona): Observable<any> {
        return this.http
                .put(this.base_path + '/PersonasFisicas/' + persona.IdPersonaFisica, persona, this.httpOptions)
                .pipe(
                    retry(2),
                    catchError(this.handleError)
                )
    }

    eliminarPersona(persona:Persona): Observable<any> {
        return this.http
                .delete(this.base_path + '/PersonasFisicas/' + persona.IdPersonaFisica, this.httpOptions)
                .pipe(
                    retry(2),
                    catchError(this.handleError)
                )
    }
}
