import { TestBed } from '@angular/core/testing';

import { ApiPersonasService } from './api-personas.service';

describe('ApiPersonasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiPersonasService = TestBed.get(ApiPersonasService);
    expect(service).toBeTruthy();
  });
});
