import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Usuario } from '../../interfaces/Usuario';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    userToken: string;

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }    

    constructor(private http: HttpClient) {
        this.getToken();
    }   

    login( usuario:Usuario ){
        return this.http.post('https://api.toka.com.mx/candidato/api/login/authenticate', usuario, this.httpOptions)
        .pipe(
            map( resp => {
                this.setToken( resp["Data"] );
                return resp;
            })
        );
    }

    setToken(token: string ){
        this.userToken = token;
        localStorage.setItem('token', token);
    }

    getToken(){
        if( localStorage.getItem('token') ){
            this.userToken = localStorage.getItem('token');
        }else{
            this.userToken = '';
        }
    }

    getAutenticado():boolean{
        return this.userToken.length > 0;
    }

    logout(){
        localStorage.removeItem('token');
    }

    getCustomers(){
        let httpOptionsToka = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.userToken
            })
        };

        return this.http.get('https://api.toka.com.mx/candidato/api/customers', httpOptionsToka);
    }
}
