import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Usuario } from 'src/app/interfaces/Usuario';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    usuario: Usuario = {
        Username: '',
        Password: ''
    };

    constructor( private auth:AuthService ) { }

    ngOnInit() {
    }

    login(form: NgForm){
        if( form.invalid ){
            return;
        }
        this.auth.login( this.usuario ).subscribe( resp => {console.log(resp);});
    }

}
