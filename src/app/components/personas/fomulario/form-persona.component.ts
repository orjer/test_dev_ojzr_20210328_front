import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Persona } from '../../../interfaces/Persona';
import { ApiPersonasService } from 'src/app/services/personas/api-personas.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-form-persona',
  templateUrl: './form-persona.component.html',
  styleUrls: ['./form-persona.component.css']
})
export class FormPersonaComponent implements OnInit {

    @Input() persona: Persona;
  	personaForm: FormGroup;
	submitted = false;
	lblModal: string;

	@Output() personaModificado: EventEmitter<Persona> = new EventEmitter<Persona>();

  	constructor(
		private http: HttpClient, 
		private apiPersonas: ApiPersonasService, 
		private fb: FormBuilder,
		private activeModal: NgbActiveModal) {			
			this.crearFormulario();
	}

	cerrar(){
		this.activeModal.close();
	}

	ngOnChanges(changes: SimpleChanges){
        console.log(changes);
        
		if(changes.persona.currentValue !== undefined){
			this.editar( changes.persona.currentValue );
		}
	}	

	crearFormulario() {
		this.personaForm = this.fb.group({
            IdPersonaFisica: ['0', Validators.required],
			Nombre: ['', [Validators.required] ],
			ApellidoPaterno: ['', [Validators.required] ],
            ApellidoMaterno: ['', [Validators.required] ],
            RFC: ['', [Validators.required, Validators.minLength(13), Validators.maxLength(13)] ],
		});
	}

	ngOnInit(): void {
		this.editar( this.persona );
	}
	  
	editar(persona?){
		this.personaForm.reset();
		this.submitted = false;
		this.lblModal = persona ? "Editar Persona" : "Registrar Persona";
		this.personaForm.setValue({
			IdPersonaFisica: persona ? persona.IdPersonaFisica : 0,
			Nombre: persona ? persona.Nombre : '',
            ApellidoPaterno: persona ? persona.ApellidoPaterno : '',
            ApellidoMaterno: persona ? persona.ApellidoMaterno : '',
            RFC: persona ? persona.RFC : ''			
		});
	}
	  
	onSubmit() {
		this.submitted = true;
		if (this.personaForm.invalid) {
			return;
		}

		this.persona = this.personaForm.value;

		Swal.fire({
			allowEscapeKey: false,
			allowOutsideClick: false,
			icon: "info",
			text: "Guardando cambios..."
		});
		Swal.showLoading();

		if(this.persona.IdPersonaFisica == 0){
			this.crearPersona();
		}else{
			this.modificarPersona();
		}
	}

	crearPersona(){
		this.apiPersonas.crearPersona(this.persona).subscribe(response => {
			Swal.close();
			var tipo;
			if(response.error){                
			    tipo = "error";
			}else{
			    tipo = "success";
			    this.activeModal.close( response );
			}
			Swal.fire({
			    allowOutsideClick: false,
			    icon: tipo,
			    text: response.mensaje
			});
		});
	}

	modificarPersona(){
		this.apiPersonas.modificarPersona(this.persona).subscribe(response => {
			Swal.close();
			var tipo;
			if(response.error){                
			    tipo = "error";
			}else{
			    tipo = "success";
			    this.activeModal.close( response );
			}
			Swal.fire({
			    allowOutsideClick: false,
			    icon: tipo,
			    text: response.mensaje
			});
		});
	}
}
