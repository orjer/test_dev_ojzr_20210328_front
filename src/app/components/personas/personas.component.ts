import { Component, OnInit, ViewChild } from '@angular/core';

import { DataTableDirective } from 'angular-datatables';
import Swal from 'sweetalert2';
import { ApiPersonasService } from 'src/app/services/personas/api-personas.service';
import { FormPersonaComponent } from 'src/app/components/personas/fomulario/form-persona.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Persona } from 'src/app/interfaces/Persona';


@Component({
    selector: 'app-personas',
    templateUrl: './personas.component.html',
    styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {
    @ViewChild(DataTableDirective)
	private datatableElement: DataTableDirective;
    
    dtOptions: any = {};
	personas: any[];
	IdPersonaFisica: number = 0;

    constructor(
        private apiPersonas: ApiPersonasService,
        private modal: NgbModal
    ) { }

    ngOnInit() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
            },
            searchDelay: 10000,
            serverSide: true,
            processing: true,
            initComplete: function(){
                $(".dt-button").removeClass("dt-button");
            },
            ajax: (dataTablesParameters: any, callback) => {
                    that.apiPersonas.getPersonas().subscribe( resp => {
                        that.personas = resp || [];
                        callback({
                            recordsTotal: resp.length,
                            recordsFiltered: resp.length,
                            data: []
                        });
                    });
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    text: 'Nuevo',
                    className: 'btn btn-outline-dark',
                    action: function (e, dt, node, config) {
                        document.getElementById("btnNuevaPersona").click();
                    }
                },
                {
                    text: 'Excel',
                    extend: "excel",
                    className: 'btn btn-outline-dark'
                }
            ],
            columns: [
                { 
                    data: '',
                    name: 'Nombre',
                    width:"20%"
                },
                { 
                    data: '',
                    name: 'ApellidoPaterno',
                    width:"20%" 
                },
                { 
                    data: '',
                    name: 'AapellidoMaterno',
                    width:"20%"
                },
                { 
                    data: '',
                    name: 'RFC',
                    width:"20%"
                },
                { 
                    data: '',
                    name: 'acciones',
                    width:"20%",
                    orderable: false,
                    searchable: false
                }
            ]
        };
    }

    editar(persona: Persona){
		const modalRef = this.modal.open(FormPersonaComponent, {
			keyboard: false,
        		backdrop: 'static'
		});        
		modalRef.componentInstance.persona = persona;
		modalRef.result.then((result) => {
            console.log("result", result);
            
			if(result){
				this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.ajax.reload(null, false);
				});
			}
		}, (reason) => {});
	}

	eliminar(persona: Persona){
		Swal.fire({
		    text: '¿Está seguro de eliminar a la persona ' + persona.Nombre + '?',
		    icon: 'question',
		    confirmButtonText: 'Sí, eliminar',
		    cancelButtonText: 'No, regresar',
		    showCancelButton: true,
		    showCloseButton: true
		}).then( (result) => {
		    if(result.value){                             
			    this.apiPersonas.eliminarPersona(persona).subscribe( response => {
				this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
					dtInstance.ajax.reload(null, false);
				});
				Swal.fire({
					allowOutsideClick: false,
					icon: "success",
					text: "Persona eliminada"
					});
			   });
		    }
		});
	}


}
