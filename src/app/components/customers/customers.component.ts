import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  @ViewChild(DataTableDirective)
	private datatableElement: DataTableDirective;
    
    dtOptions: any = {};
	customers: any[];

    constructor(
        private apiAuth: AuthService
    ) { }

    ngOnInit() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10,
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
            },
            searchDelay: 10000,
            serverSide: true,
            processing: true,
            initComplete: function(){
                $(".dt-button").removeClass("dt-button");
            },
            ajax: (dataTablesParameters: any, callback) => {
                    that.apiAuth.getCustomers().subscribe( resp => {
                        that.customers = resp["Data"] || [];
                        callback({
                            recordsTotal: resp["Data"].length,
                            recordsFiltered: resp["Data"].length,
                            data: []
                        });
                    });
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    text: 'Excel',
                    extend: "excel",
                    className: 'btn btn-outline-dark'
                }
            ],
            columns: [
                { 
                    data: '',
                    name: 'RazonSocial',
                    width:"20%"
                },
                { 
                    data: '',
                    name: 'RFC',
                    width:"20%" 
                },
                { 
                    data: '',
                    name: 'Sucursal',
                    width:"20%"
                }
            ]
        };
    }

}
