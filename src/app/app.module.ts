import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PersonasComponent } from './components/personas/personas.component';
import { FormPersonaComponent } from './components/personas/fomulario/form-persona.component';

import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule }   from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './components/login/login.component';
import { CustomersComponent } from './components/customers/customers.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PersonasComponent,
    FormPersonaComponent,
    LoginComponent,
    CustomersComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    DataTablesModule
  ],
  entryComponents:[FormPersonaComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
