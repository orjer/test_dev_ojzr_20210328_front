export interface Customer {
    IdCliente: number,
    FechaRegistroEmpresa: any;
    RazonSocial: String,
    RFC: String,
    Sucursal: String,
    IdEmpleado: number,
    Nombre: String,
    Paterno: String,
    Materno: String,
    IdViaje: number
}
