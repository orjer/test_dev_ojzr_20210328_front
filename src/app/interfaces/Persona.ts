export interface Persona {
    IdPersonaFisica: number,
    FechaRegistro: any;
    FechaActualzacion: any,
    Nombre: String,
    ApellidoPaterno: String,
    ApellidoMaterno: String,
    RFC: String,
    FechaNacimiento: any,
    UsuarioAgrega: number,
    Activo: number
}
